package org.liuxp.minioplus.core.common.utils;

import cn.hutool.core.date.LocalDateTimeUtil;

/**
 * 对象存储工具类
 * @author contact@liuxp.me
 * @since  2024/05/23
 */
public class CommonUtil {

    /**
     * 取得对象名称
     * @param md5 文件MD5值
     * @return 对象名称
     */
    public static String getObjectName(String md5){
        return CommonUtil.getPathByDate() + "/" + md5;
    }

    /**
     * 根据当前时间取得路径
     * @return 路径
     */
    public static String getPathByDate(){
        return LocalDateTimeUtil.format(LocalDateTimeUtil.now(), "yyyy/MM");
    }

}
